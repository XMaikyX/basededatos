﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using InterfazUleam.Modelos;

namespace InterfazUleam
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main( string[] args)
        {
            /*Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormLogin());*/

        }
        static void ListadoProfesores()
        {
            using(var db= new EntityFrameworkContext())
            {
                List<Profesor> listadoProfesores = db.Profesors.ToList();
                foreach(var profesor in listadoProfesores)
                {
                    Console.WriteLine(profesor.Apellido);
                }
            }
        }
        static void IngresarProfesor()
        {
            using (var db= new EntityFrameworkContext())
            {
                Profesor profesor = new Profesor();
                profesor.Nombre = "Michael";
                profesor.Apellido = "Penafiel";

                db.Add(profesor);
                db.SaveChanges();

            }
            return;
        }
        static void ModificarProfesor()
        {
            using (var db = new EntityFrameworkContext())
            {
                Profesor profesor = db.Profesors.Find(5);
                profesor.Nombre = "Leonardo";
                db.SaveChanges();
                
            }
        }
        static void EliminarProfesor()
        {
            using (var db = new EntityFrameworkContext())
            {
                Profesor profesor = new Profesor();
                db.Profesors.Remove(profesor);
                db.SaveChanges();
            }
        }
    }
}
